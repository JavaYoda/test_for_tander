package ru.testproject.writers;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by User on 08.10.2017.
 */
public class TestDataXmlDOMWriter {

    public static void write(List<Long> list, String fileName){

        DocumentBuilderFactory dbf;
        DocumentBuilder documentBuilder;
        Document document = null;

        try {
            dbf = DocumentBuilderFactory.newInstance();
            documentBuilder = dbf.newDocumentBuilder();
            document = documentBuilder.newDocument();

            Element root_entries = document.createElement("entries");
            document.appendChild(root_entries);

            for(long i : list){
                Element entry = document.createElement("entry");
                Element field = document.createElement("field");
                entry.appendChild(field);
                field.setTextContent(String.valueOf(i));
                root_entries.appendChild(entry);
            }

        }
        catch(ParserConfigurationException e){
            e.printStackTrace();
        }
        finally {
            if(document != null)
                writeDocument(document, fileName);
        }
    }

    private static void writeDocument(Document document, final String path)
            throws TransformerFactoryConfigurationError
    {
        Transformer trf;
        DOMSource src;
        FileOutputStream fos;
        try {
            trf = TransformerFactory.newInstance().newTransformer();
            src = new DOMSource(document);
            fos = new FileOutputStream(path);

            StreamResult result = new StreamResult(fos);
            trf.transform(src, result);
        } catch (TransformerException e) {
            e.printStackTrace(System.out);
        } catch (IOException e) {
            e.printStackTrace(System.out);
        }
    }
}
