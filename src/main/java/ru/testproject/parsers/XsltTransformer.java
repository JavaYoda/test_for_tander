package ru.testproject.parsers;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 * Created by User on 09.10.2017.
 */
public class XsltTransformer {

    public static void transform(String inputFile, String outputFile, String template)
            throws TransformerException {

        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer =
                factory.newTransformer(new StreamSource(template));
        transformer.transform(new StreamSource(inputFile),
                new StreamResult(outputFile));

    }



}
