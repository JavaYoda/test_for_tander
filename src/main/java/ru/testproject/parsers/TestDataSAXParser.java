package ru.testproject.parsers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by User on 08.10.2017.
 */
public class TestDataSAXParser {

    private static Logger logger = LoggerFactory.getLogger(TestDataSAXParser.class);

    private ArrayList<Long> result;

    private DefaultHandler testDataHandler;

    public TestDataSAXParser(){
        result = new ArrayList<Long>();
        testDataHandler = new DefaultHandler() {
            @Override
            public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

                int length = attributes.getLength();

                for (int i = 0; i < length; i++) {
                    result.add(Long.valueOf(attributes.getValue(i)));
                }
            }
        };
    }

    public TestDataSAXParser(ArrayList<Long> result, DefaultHandler defaultHandler) {
        this.result = result;
        this.testDataHandler = defaultHandler;
    }

    public DefaultHandler getDefaultHandler() {
        return testDataHandler;
    }

    public void setDefaultHandler(DefaultHandler defaultHandler) {
        this.testDataHandler = defaultHandler;
    }

    public ArrayList<Long> parse(String xmlFile) throws TestDataSAXParserException {
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        saxParserFactory.setValidating(false);
        SAXParser saxParser;
        try {
            saxParser = saxParserFactory.newSAXParser();
            saxParser.parse(new File(xmlFile), testDataHandler);
        } catch (ParserConfigurationException e) {
            logger.error("Error parsing test data", e);
            throw new TestDataSAXParserException("Error parsing test data", e);
        } catch (SAXException e) {
            logger.error("Error parsing test data", e);
            throw new TestDataSAXParserException("Error parsing test data", e);
        } catch (IOException e) {
            logger.error("Error parsing test data", e);
            throw new TestDataSAXParserException("Error parsing test data", e);
        }
        return result;
    }
}
