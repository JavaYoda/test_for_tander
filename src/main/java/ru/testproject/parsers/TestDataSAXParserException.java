package ru.testproject.parsers;

/**
 * Created by User on 10.10.2017.
 */
public class TestDataSAXParserException extends Exception{

    public TestDataSAXParserException() {
    }

    public TestDataSAXParserException(String message) {
        super(message);
    }

    public TestDataSAXParserException(String message, Throwable cause) {
        super(message, cause);
    }

    public TestDataSAXParserException(Throwable cause) {
        super(cause);
    }
}
