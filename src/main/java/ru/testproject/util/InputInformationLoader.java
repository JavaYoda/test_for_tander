package ru.testproject.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.testproject.config.DBConfig;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by User on 10.10.2017.
 */
public class InputInformationLoader {

    private String writeXMLFile;
    private String convertedXMLFile;
    private String xlsTemplate;
    private int batchSize;
    private long N;

    private static Logger logger = LoggerFactory.getLogger(DBConfig.class);

    public InputInformationLoader() {loadDBProperties();}

    private void loadDBProperties() {
        try {
            Properties properties = new Properties();
            properties.load(InputInformationLoader.class.getClassLoader()
                    .getResourceAsStream("inputInformation.properties"));
            writeXMLFile = properties.getProperty("XML_WRITE_FILE");
            convertedXMLFile = properties.getProperty("CONVERTED_XML_FILE");
            xlsTemplate = properties.getProperty("XLS_TEMPLATE");
            batchSize = Integer.valueOf(properties.getProperty("BATCH_SIZE"));
            N = Long.valueOf(properties.getProperty("N"));
        }
        catch (IOException e){
            logger.error("Input information config reading error:", e);
        }
    }

    public String getWriteXMLFile() {
        return writeXMLFile;
    }

    public String getConvertedXMLFile() {
        return convertedXMLFile;
    }

    public int getBatchSize() {
        return batchSize;
    }

    public String getXlsTemplate() {
        return xlsTemplate;
    }

    public long getN() {
        return N;
    }
}
