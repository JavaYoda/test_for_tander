package ru.testproject.repo;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class JdbcTestDataRepo extends JdbcBaseRepo {

    private int batchSize = 10000;

    public JdbcTestDataRepo() {}

    public JdbcTestDataRepo(DataSource dataSource) {
        super(dataSource);
    }

    public void deleteAll() throws SQLException {
        Connection connection = null;
        try {
            connection = getConnection();
            Statement statement = connection.createStatement();
            statement.execute("DELETE FROM test");
        }
        catch (SQLException e){
            logger.error("Error cleaning the database", e);
            throw e;
        }
        finally {
            closeConnection(connection);
        }
    }

    public void add(long N) throws SQLException {
        Connection connection = null;
        try {
            connection = getConnection();
            PreparedStatement ps = connection.prepareStatement("INSERT INTO test(data) VALUES (?)");
            ps.setLong(1, N);
            ps.executeUpdate();
        }
        catch (SQLException e){
            logger.error("Error add into database", e);
            throw e;
        }
        finally {
            closeConnection(connection);
        }
    }

    public void addBatch(long N) throws SQLException {
        Connection connection = null;
        try {
            connection = getConnection();
            connection.setAutoCommit(false);

            PreparedStatement ps = connection.prepareStatement(" INSERT INTO test(data) VALUES (?)");

            int batchIndex = 0;
            int dataIndex = 0;
            for(long data = 1; data<=N; data++){
                ps.setLong(1, data);
                ps.addBatch();

                if(++batchIndex % batchSize == 0)
                    ps.executeBatch();
            }

            ps.executeBatch();
            connection.commit();
        }
        catch (SQLException e){
            if(connection !=null) connection.rollback();
            logger.error("Error add batch into database", e);
            throw e;
        }
        finally {
            closeConnection(connection);
        }
    }

    public List<Long> getAll() throws SQLException {

        ArrayList<Long> list = new ArrayList<Long>();

        Connection connection = null;
        try {
            connection = getConnection();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM test");

            while(rs.next()){
                list.add(rs.getLong("data"));
            }
        }
        catch (SQLException e){
            logger.error("Error select from database", e);
            throw e;
        }
        finally {
            closeConnection(connection);
        }
        return list;
    }

    public void setBatchSize(int batchSize) {
        this.batchSize = batchSize;
    }
}
