package ru.testproject.repo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class JdbcBaseRepo {

    protected Logger logger = LoggerFactory.getLogger(JdbcBaseRepo.class);

    private DataSource dataSource;

    public JdbcBaseRepo(){}

    public JdbcBaseRepo(DataSource dataSource) { this.dataSource = dataSource; }


    protected Connection getConnection() throws SQLException {return dataSource.getConnection();}

    protected void closeConnection(Connection connection){
        try
        {
            if(connection != null) {
                connection.close();
                logger.debug("Database connection is closed.");
            }
        }
        catch (SQLException e) {
            logger.error("Closing database connection error: ", e);
        }
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}
