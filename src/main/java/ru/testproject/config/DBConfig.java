package ru.testproject.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class DBConfig {

    private String url;
    private String user;
    private String password;
    private String driver;

    private static Logger logger = LoggerFactory.getLogger(DBConfig.class);



    public DBConfig() { loadDBProperties(); }

    private void loadDBProperties() {
        try {
            Properties properties = new Properties();
            properties.load(DBConfig.class.getClassLoader().getResourceAsStream("configDB.properties"));
            url = properties.getProperty("DATABASE_URL");
            user = properties.getProperty("DATABASE_USER");
            password = properties.getProperty("DATABASE_PASSWORD");
            driver = properties.getProperty("DATABASE_DRIVER");
        }
        catch (IOException e){
            logger.error("Database config reading error:", e);
        }
    }

    public String getUrl() {
        return url;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public String getDriver() { return driver; }
}
