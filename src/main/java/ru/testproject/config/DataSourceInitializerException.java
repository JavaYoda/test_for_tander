package ru.testproject.config;

public class DataSourceInitializerException extends Exception {

    public DataSourceInitializerException() {super();}

    public DataSourceInitializerException(String message) { super(message); }

    public DataSourceInitializerException(Throwable cause) { super(cause); }

    public DataSourceInitializerException(String message, Throwable cause) { super(message, cause); }


}
