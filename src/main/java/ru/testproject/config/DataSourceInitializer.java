package ru.testproject.config;

import org.sqlite.SQLiteDataSource;

import javax.sql.DataSource;

public class DataSourceInitializer {

    private DBConfig dbConfig;

    public DataSourceInitializer(DBConfig dbConfig) {
        this.dbConfig = dbConfig;
    }

    public DataSource getDataSource() throws DataSourceInitializerException {

        DataSource dataSource = null;
        String driver = dbConfig.getDriver();

        if(driver.contains("sqlite"))
            dataSource = getSQLiteDataSource();

        if(dataSource == null)
            throw new DataSourceInitializerException("Unsupported driver" + driver);

        return dataSource;
    }

    private DataSource getSQLiteDataSource() {
        SQLiteDataSource dataSource = new SQLiteDataSource();
        dataSource.setUrl(dbConfig.getUrl());
        return dataSource;
    }

}
