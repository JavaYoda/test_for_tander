package ru.testproject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.testproject.config.DBConfig;
import ru.testproject.config.DataSourceInitializer;
import ru.testproject.config.DataSourceInitializerException;
import ru.testproject.parsers.TestDataSAXParser;
import ru.testproject.parsers.TestDataSAXParserException;
import ru.testproject.parsers.XsltTransformer;
import ru.testproject.repo.JdbcTestDataRepo;
import ru.testproject.services.TestDataService;
import ru.testproject.services.TestDataServiceException;
import ru.testproject.services.TestDataServiceImpl;
import ru.testproject.util.InputInformationLoader;
import ru.testproject.writers.TestDataXmlDOMWriter;

import javax.xml.transform.TransformerException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Starter {

    private static Logger logger = LoggerFactory.getLogger(Starter.class);

    public static void main(String[] args) {

        InputInformationLoader loader = new InputInformationLoader();
        DBConfig dbConfig = new DBConfig();
        DataSourceInitializer dataSourceInitializer = new DataSourceInitializer(dbConfig);
        try {

            ArrayList<Long> list;
            JdbcTestDataRepo repo = new JdbcTestDataRepo(dataSourceInitializer.getDataSource());
            repo.setBatchSize(loader.getBatchSize());
            TestDataService testDataService = new TestDataServiceImpl(repo);
            System.out.println(new Date());
            testDataService.deleteAll();
            testDataService.addAll(loader.getN());
            List<Long> temp = new ArrayList<Long>();
            temp = testDataService.getAll();
            TestDataXmlDOMWriter.write(testDataService.getAll(), loader.getWriteXMLFile());
            XsltTransformer.transform(loader.getWriteXMLFile(), loader.getConvertedXMLFile(), loader.getXlsTemplate());
            TestDataSAXParser parser = new TestDataSAXParser();
            list = parser.parse(loader.getConvertedXMLFile());
            long sum = 0;
            for (long i: list) {
                sum+=i;
            }
            System.out.println("Result sum: " + sum);
            System.out.println(new Date());

        } catch (DataSourceInitializerException e) {
            e.printStackTrace();
        }
         catch (TestDataServiceException e) {
            logger.error("Test data service error", e);
        } catch (TransformerException e) {
            logger.error("Transforming error", e);
        } catch (TestDataSAXParserException e) {
            logger.error("Parsing error", e);
        }
    }
}
