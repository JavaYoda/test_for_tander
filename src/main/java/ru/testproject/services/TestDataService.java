package ru.testproject.services;

import java.util.List;

public interface TestDataService {
    void add(long number) throws TestDataServiceException;
    void addAll(long N) throws TestDataServiceException;
    List<Long> getAll() throws TestDataServiceException;
    void deleteAll() throws TestDataServiceException;
}
