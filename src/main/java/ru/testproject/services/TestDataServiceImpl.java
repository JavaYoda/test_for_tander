package ru.testproject.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.testproject.repo.JdbcTestDataRepo;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by User on 08.10.2017.
 */
public class TestDataServiceImpl implements TestDataService {

    protected Logger logger = LoggerFactory.getLogger(TestDataServiceImpl.class);

    private JdbcTestDataRepo jdbcTestDataRepo;

    public TestDataServiceImpl() {}

    public TestDataServiceImpl(JdbcTestDataRepo jdbcTestDataRepo) {
        this.jdbcTestDataRepo = jdbcTestDataRepo;
    }


    public void add(long number) throws TestDataServiceException{
        try {
            jdbcTestDataRepo.deleteAll();
            jdbcTestDataRepo.add(number);
        } catch (SQLException e) {
            throw new TestDataServiceException("Data inserting error:", e);
        }
    }

    public void addAll(long N) throws TestDataServiceException{
        try {
            jdbcTestDataRepo.addBatch(N);
        } catch (SQLException e) {
            throw new TestDataServiceException("Data inserting error:", e);
        }
    }

    public List<Long> getAll() throws TestDataServiceException{
        try {
            return jdbcTestDataRepo.getAll();
        } catch (SQLException e) {
            throw new TestDataServiceException("Data selecting error:", e);
        }
    }


    public void deleteAll() throws TestDataServiceException{
        try {
            jdbcTestDataRepo.deleteAll();
        } catch (SQLException e) {
            throw new TestDataServiceException("Data deleting error:", e);
        }
    }

    public void setJdbcTestDataRepo(JdbcTestDataRepo jdbcTestDataRepo) {
        this.jdbcTestDataRepo = jdbcTestDataRepo;
    }

    public JdbcTestDataRepo getJdbcTestDataRepo() {
        return jdbcTestDataRepo;
    }
}
