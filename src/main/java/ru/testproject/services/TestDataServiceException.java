package ru.testproject.services;

/**
 * Created by User on 08.10.2017.
 */
public class TestDataServiceException extends Exception {

    public TestDataServiceException() {
    }

    public TestDataServiceException(String message) {
        super(message);
    }

    public TestDataServiceException(Throwable cause) {
        super(cause);
    }

    public TestDataServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
